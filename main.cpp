// Collier, Jesse
// CIS1111 Final
// Aquarium Monitor 1.0
// 2014-12-09
// A program to monitor an aquarium
// All works herein property of Jesse Q. Collier and may not be distributed without express written permission.

#include <iostream>	// for io operations
#include <iomanip>	// for formatting output to look nice
#include <ctime>	// To include time in output file and seed PRNG
#include <string>	// For user input
#include <cstring>	// to convert strings to numbers
#include <fstream>	// For writing to file
#include <cmath>	// For grading and maths stuff
#include <cstdlib>	// For rand and srand
#include <cctype>	// For advanced string functions

// Function Prototypes
char menuValidation();										// data validation for menu input
void titleFunction();										// show the title bar
void tempFunction(std::string[], std::string[], int, double *);		// temperature function
bool testTemp(std::string);									// temperature input validation
void pHFunction(std::string[], std::string[], int, double *);			// pH function
bool testpH(std::string);									// pH input validation
void ammoniaFunction(std::string[], std::string[], int, double *);	// ammonia function
bool testAmmonia(std::string);								// ammonia input validation
void nitriteFunction(std::string[], std::string[], int, double *);	// nitrite function
bool testNitrite(std::string);								// nitrite input validation
void nitrateFunction(std::string[], std::string[], int, int *);	// nitrate function
bool testNitrate(std::string);								// nitrate input validation
void filterFunction(std::string[], std::string[], int);		// filter function
bool testFilter(std::string);								// filter input validation
void gallonsFunction(std::string[], std::string[], int, double *, double *, double *);	// water change function
bool testGallons(std::string);								// water change validation
void journalFunction(std::string[], std::string[], int);	// journal to record objective data
void fsOutputFunction(std::string[], int);					// write data to file
void gradeFunction(double, double, double, double, int, std::string[], int, double, double, double);			// grade data
void showHelp(std::string[], int);							// displays help on main screen
void artRoll();												// displays art based on PRNG
void art0();
void artUpper0();
void artMiddle0();
void artMiddle1();
void artMiddle2();
void artBottom0();
void artBottom1();
void artBottom2();

// debug function to find errors in non-displayed data array
// uncomment protytype, function, and function call in main() to cause default switch case to return values stored in array written to file
// void debugDataArrayFunction(std::string[], int);

// Constants
const int MAIN_MENU = 11;								// ten menu options, ten data sets collected
const int ART_MIN = 1, ART_MAX = 3;						// there are six peices of art to be displayed
const double MIN_TEMPF = 60.0, MAX_TEMPF = 90.0;		// Temp must be between 60 and 90 degrees farenheit
const double MIN_PH = 6.0, MAX_PH = 8.8;				// pH must be between 6.0 and 8.8 percent Hydrogen
const double MIN_AMMONIA = 0.00, MAX_AMMONIA = 8.00;	// Ammonia must be between 0.0 and 8.0 ppm
const double MIN_NITRITE = 0.00, MAX_NITRITE = 5.00;	// Nitrite must be between 0.0 and 5.0 ppm
const int MIN_NITRATE = 0, MAX_NITRATE = 160;			// Nitrate must be between 0 and 160 ppm

const double TEMP_IDEAL = 77.7;							// the "ideal" temperature
const double PH_IDEAL = 7.8;							// the "ideal" pH
const double IDEAL_WATER_CHANGE = 0.4;					// the ideal weekly water change

// Main Program
int main()
{
	int count = 0;										// counter to hold display sequence
	std::string mainMenuDisplay[MAIN_MENU] = { "1 - Temperature\t", "2 - pH\t\t", "3 - Ammonia\t", "4 - Nitrite\t", "5 - Nitrate\t", "6 - Filter\t", "7 - Water Change", "8 - Record Notes", "9 - Write Data\t", "? - Help\t", "Q - Exit\t" };	// user menu options
	std::string dataDisplay[MAIN_MENU] = { "No Data", "No Data", "No Data", "No Data", "No Data", "No Data", "No Data", "No Data", " ", " " , " "};	// data from current entry
	std::string fileOutput[MAIN_MENU];					// data to write to file is stored here
	char menuChoice;									// char data will be used to operate switch

	double tempDouble;									// address passed to temp function
	double pHDouble;									// address passed to pH function
	double ammoniaDouble;								// address passed to ammonia function
	double nitriteDouble;								// address passed to nitrite function
	int nitrateInt;										// address passed to nitrate function
	double gallonsTotalDouble = 0;							// stores total gallons			// address passed to water change function
	double gallonsChangedDouble = 0;						// stores gallons changed		// address passed to water change function
	double percentWaterChange = 0;							// stores percentage of change	// address passed to water change function

	do	// begin main loop
	{
		titleFunction();	// show the title
		std::cout << "\tMenu Options\t\tCurrent Data" << std::endl << std::endl;	// explanation of display


		for (count = 0; count < MAIN_MENU; count++)	// display main menu and previous and current data sets
		{
			std::cout << "\t" << mainMenuDisplay[count] << "\t";	// display user options
			std::cout << dataDisplay[count] << std::endl;			// display current data ready to record
		}

		menuChoice = menuValidation();	// user input for menu and input validation

		// Begin menu action with valid menu option input
		switch (menuChoice)
		{

			//	Temperature
		case '1':
			artRoll();	// dynamicish art
			tempFunction(dataDisplay, fileOutput, MAIN_MENU, &tempDouble);		// calls temperature function
			break;

			//	pH
		case '2':
			artRoll();	// dynamicish art
			pHFunction(dataDisplay, fileOutput, MAIN_MENU, &pHDouble);			// calls pH function
			break;

			//	Ammonia
		case '3':
			artRoll();	// dynamicish art
			ammoniaFunction(dataDisplay, fileOutput, MAIN_MENU, &ammoniaDouble);	// calls ammonia function
			break;

			//	Nitrite
		case '4':
			artRoll();	// dynamicish art
			nitriteFunction(dataDisplay, fileOutput, MAIN_MENU, &nitriteDouble);	// calls nitrite function
			break;

			//	Nitrate
		case '5':
			artRoll();	// dynamicish art
			nitrateFunction(dataDisplay, fileOutput, MAIN_MENU, &nitrateInt);	// calls nitrate function
			break;

			//	Filter
		case '6':
			artRoll();	// dynamicish art
			filterFunction(dataDisplay, fileOutput, MAIN_MENU);	// calls filter function
			break;

			//	Gallons Changed
		case '7':
			artRoll();	// dynamicish art
			gallonsFunction(dataDisplay, fileOutput, MAIN_MENU, &gallonsTotalDouble, &gallonsChangedDouble, &percentWaterChange);	// calls gallons changed function
			break;

			//	Journal Entry
		case '8':
			artRoll();	// dynamicish art
			journalFunction(dataDisplay, fileOutput, MAIN_MENU);	// calls journal function
			break;

			//	Output to File and score
		case '9':
			artRoll();	// dynamicish art
			fsOutputFunction(fileOutput, MAIN_MENU);	// call fstream function to write new data
			std::cout << std::endl;
			gradeFunction(tempDouble, pHDouble, ammoniaDouble, nitriteDouble, nitrateInt, dataDisplay, MAIN_MENU, gallonsTotalDouble, gallonsChangedDouble, percentWaterChange);
			break;

			//	Exit Program
		case '?':
			showHelp(dataDisplay, MAIN_MENU);
			break;
		case 'Q':
			art0();
			art0();
			exit(0);	// program terminates
			break;

			// Catch all for invalid main menu selections of correct format
		default: std::cout << "invalid selection" << std::endl;
			//	debugDataArrayFunction(fileOutput, MAIN_MENU);	// uncomment function call, prototype, and function to cause default to show values stored in fileOutput[]
			system("pause");

		}
		system("cls");	// clear screen to refresh data displayed at main menu
	} while (menuChoice != 'Q');
	std::cout << "Exiting outside of loop" << std::endl;		// program should exit through switch. this is for debugging
	system("pause");
	return 0;
}

// Functions

// Title Display
void titleFunction()
{
	// this should display properly within a command prompt window with default setting of 80 char width
	std::cout << " ------------------------------------------------------------------------------" << std::endl; // 79 chars wide
	std::cout << " --------------------         Aquarium Monitor 1.0         --------------------" << std::endl; // 20/text/20
	std::cout << " --------------------           By Jesse Collier           --------------------" << std::endl; // 20/text/20
	std::cout << " ------------------------------------------------------------------------------" << std::endl << std::endl; // 79 chars wide
}

// Menu input and character return
char menuValidation()
{
	std::string menuInput;									// holds user input
	char menuValid = '0';										// holds '0' until data entered is validated

	while (menuValid = '0')
	{
		std::cout << std::endl << "\tYour selection: ";
		getline(std::cin, menuInput);						// accept any user input as string
		if (menuInput.length() != 1)						// test for input greater than one char in length
		{
			break;											// break and return '0' if longer than one char
		}
		else
		{
			menuValid = menuInput.front();					// returns first character of string into menu, incase buffer, so doesn't dump into other inputs.
			menuValid = toupper(menuValid);					// returns an uppercase character because reasons
			return menuValid;								// return good data to main()
		}
	}
	return '0';
}

// Temperature function
void tempFunction(std::string tempDataDisplay[], std::string tempFileOutput[], int temp, double *tempAddress)
{
	std::string tempInput;									// holds user input
	double tempInputDouble;									// this will be checked to see if within range

	do
	{
		std::cout << "Enter temperature in Farenheit from " << MIN_TEMPF << " to " << MAX_TEMPF << " , using the format \"77.8\": " << std::endl;
		getline(std::cin, tempInput);						// accept any user input as string

		while (!testTemp(tempInput))						// will not let user proceed until input is correct format
		{
			std::cout << "Error, invalid format.  Please use format such as \"77.8\"." << std::endl;
			std::cout << "Enter temperature in Farenheit from " << MIN_TEMPF << " to " << MAX_TEMPF << " , using the format \"77.8\": " << std::endl;
			getline(std::cin, tempInput);					// accept any user input as string
		}

		tempInputDouble = atof(tempInput.c_str());			// convert string values, of correct format, to double to check range

		if ((tempInputDouble > MAX_TEMPF) || (tempInputDouble < MIN_TEMPF))		// check range
			std::cout << "Error, outside range.  Temp must be between 60 and 90 degrees farenheit." << std::endl;

	} while ((tempInputDouble > MAX_TEMPF) || (tempInputDouble < MIN_TEMPF));	// exit if within range
	*tempAddress = tempInputDouble;							// the address of tempDouble in main()
	tempFileOutput[1] = tempInput.c_str();					// return user input to fileOutput[1]
	tempDataDisplay[0] = tempInput.c_str();					// return user input to dataDisplay[0]
}
// Temp format validation
bool testTemp(std::string inputTest)
{
	if (inputTest.length() == 4)							// test for four chars in length
		if (isdigit(inputTest.at(0)))						// test for first char is digit
			if (isdigit(inputTest.at(1)))					// test for second char is digit
				if (inputTest.at(2) == '.')					// test for third char is '.'
					if (isdigit(inputTest.at(3)))			// test for fourth char is digit
						return true;						// return true
					else return false;
				else return false;
			else return false;
		else return false;
	else return false;

}

// pH function
void pHFunction(std::string pHDataDisplay[], std::string pHFileOutput[], int pH, double *pHAddress)
{
	std::string pHInput;									// holds user input
	double pHInputDouble = 0;								// this will be checked to see if within range


	do
	{
		std::cout << "Enter pH from " << MIN_PH << " to " << MAX_PH << " , using the format \"7.8\": " << std::endl;
		getline(std::cin, pHInput);							// accept any user input as string

		while (!testpH(pHInput))							// will not let user proceed until input is correct format
		{
			std::cout << "Error, invalid format.  Please use format such as \"7.8\"." << std::endl;
			std::cout << "Enter pH from " << MIN_PH << " to " << MAX_PH << " , using the format \"7.8\": " << std::endl;
			getline(std::cin, pHInput);						// accept any user input as string
		}

		pHInputDouble = atof(pHInput.c_str());				// convert string values, of correct format, to double to check range

		if ((pHInputDouble > MAX_PH) || (pHInputDouble < MIN_PH))	// check range
			std::cout << "Error, outside range.  pH must be between 6.0 and 8.8 percent Hydrogen." << std::endl;

	} while ((pHInputDouble > MAX_PH) || (pHInputDouble < MIN_PH));	// exit if within range
	*pHAddress = pHInputDouble;								// the address of pHDouble in main() receives double value
	pHFileOutput[2] = pHInput.c_str();						// return user input to outputFile[2]
	pHDataDisplay[1] = pHInput.c_str();						// return user input to dataDisplay[1]

}
// pH format validation
bool testpH(std::string inputTest)
{
	if (inputTest.length() == 3)							// test for three chars in length
		if (isdigit(inputTest.at(0)))						// test for first char is digit
			if (inputTest.at(1) == '.')						// test for second char is '.'
				if (isdigit(inputTest.at(2)))				// test for third char is digit
					return true;							// return true
				else return false;
			else return false;
		else return false;
	else return false;

}

// Ammonia function
void ammoniaFunction(std::string ammoniaDataDisplay[], std::string ammoniaFileOutput[], int ammonia, double *ammoniaAddress)
{
	std::string ammoniaInput;								// holds user input
	double ammoniaInputDouble = 0;							// this will be checked to see if within range


	do
	{
		std::cout << "Enter ammonia in PPM from " << MIN_AMMONIA << " to " << MAX_AMMONIA << " , using the format \"0.00\": " << std::endl; // This endl makes me sad.
		getline(std::cin, ammoniaInput);					// accept any user input as string

		while (!testAmmonia(ammoniaInput))					// will not let user proceed until input is correct format
		{
			std::cout << "Error, invalid format.  Please use format such as \"0.00\"." << std::endl;
			std::cout << "Enter ammonia in PPM from " << MIN_AMMONIA << " to " << MAX_AMMONIA << " , using the format \"0.00\": " << std::endl; // This endl makes me sad.
			getline(std::cin, ammoniaInput);				// accept any user input as string
		}
		ammoniaInputDouble = atof(ammoniaInput.c_str());	// convert string values, of correct format, to double to check range

		if ((ammoniaInputDouble > MAX_AMMONIA) || (ammoniaInputDouble < MIN_AMMONIA))		// check range
			std::cout << "Error, outside range.  Ammonia must be between 0.00 and 8.00." << std::endl;

	} while ((ammoniaInputDouble > MAX_AMMONIA) || (ammoniaInputDouble < MIN_AMMONIA));		// exit if within range
	*ammoniaAddress = ammoniaInputDouble;					// the address of ammoniaDouble in main() receives double value
	ammoniaFileOutput[3] = ammoniaInput.c_str();			// return user input to outputFile[3]
	ammoniaDataDisplay[2] = ammoniaInput.c_str();			// return user input to dataDisplay[2]
}
// Ammonia format validation
bool testAmmonia(std::string inputTest)
{
	if (inputTest.length() == 4)							// test for four chars in length
		if (isdigit(inputTest.at(0)))						// test for first char is digit
			if (inputTest.at(1) == '.')						// test for second char is '.'
				if (isdigit(inputTest.at(2)))				// test for third char is digit
					if (isdigit(inputTest.at(3)))			// test for third char is digit
						return true;						// return true
					else return false;
				else return false;
			else return false;
		else return false;
	else return false;

}

// Nitrite function
void nitriteFunction(std::string nitriteDataDisplay[], std::string nitriteFileOutput[], int nitrite, double *nitriteAddress)
{
	std::string nitriteInput;								// holds user input
	double nitriteInputDouble = 0;							// this will be checked to see if within range


	do
	{
		std::cout << "Enter nitrite in PPM from " << MIN_NITRITE << " to " << MAX_NITRITE << " , using the format (0.00): " << std::endl;
		getline(std::cin, nitriteInput);					// accept any user input as string

		while (!testNitrite(nitriteInput))					// will not let user proceed until input is correct format
		{
			std::cout << "Error, invalid format.  Please use format such as \"0.00\"." << std::endl;
			std::cout << "Enter nitrite in PPM from " << MIN_NITRITE << " to " << MAX_NITRITE << " , using the format (0.00): " << std::endl;
			getline(std::cin, nitriteInput);				// accept any user input as string
		}

		nitriteInputDouble = atof(nitriteInput.c_str());	// convert string values, of correct format, to double to check range

		if ((nitriteInputDouble > MAX_NITRITE) || (nitriteInputDouble < MIN_NITRITE))		// check range
			std::cout << "Error, outside range.  Nitrite must be between 0.00 and 5.00." << std::endl;

	} while ((nitriteInputDouble > MAX_NITRITE) || (nitriteInputDouble < MIN_NITRITE));		// exit if within range
	*nitriteAddress = nitriteInputDouble;					// the address of nitriteDouble in main() receives double value
	nitriteFileOutput[4] = nitriteInput.c_str();			// return user input to outputFile[2]
	nitriteDataDisplay[3] = nitriteInput.c_str();			// return user input to dataDisplay[3]
}
// Nitrite format validation
bool testNitrite(std::string inputTest)
{
	if (inputTest.length() == 4)							// test for four chars in length
		if (isdigit(inputTest.at(0)))						// test for first char is digit
			if (inputTest.at(1) == '.')						// test for second char is '.'				
				if (isdigit(inputTest.at(2)))				// test for third char is digit
					if (isdigit(inputTest.at(3)))			// test for fourth char is digit
						return true;						// return true
					else return false;
				else return false;
			else return false;
		else return false;
	else return false;

}

// Nitrate function
void nitrateFunction(std::string nitrateDataDisplay[], std::string nitrateFileOutput[], int nitrate, int *nitrateAddress)
{
	std::string nitrateInput;								// holds user input
	int nitrateInputInt = 0;								// this will be checked to see if within range


	do
	{
		std::cout << "Enter nitrate in PPM from " << MIN_NITRATE << " to " << MAX_NITRATE << " , using the format (000): " << std::endl; // This endl makes me sad.
		getline(std::cin, nitrateInput);					// accept any user input as string

		while (!testNitrate(nitrateInput))					// will not let user proceed until input is correct format
		{
			std::cout << "Error, invalid format.  Please use format such as \"000\"." << std::endl;
			std::cout << "Enter nitrate in PPM from " << MIN_NITRATE << " to " << MAX_NITRATE << " , using the format (000): " << std::endl; // This endl makes me sad.
			getline(std::cin, nitrateInput);				// accept any user input as string
		}

		nitrateInputInt = atoi(nitrateInput.c_str());	// convert string values, of correct format, to int to check range

		if ((nitrateInputInt > MAX_NITRATE) || (nitrateInputInt < MIN_NITRATE))		// check range
			std::cout << "Error, outside range.  Nitrate must be between 0 and 160." << std::endl;

	} while ((nitrateInputInt > MAX_NITRATE) || (nitrateInputInt < MIN_NITRATE));		// exit if within range
	*nitrateAddress = nitrateInputInt;						// the address of nitriteInt in main() receives int value
	nitrateFileOutput[5] = nitrateInput.c_str();			// return user input to outputFile[5]
	nitrateDataDisplay[4] = nitrateInput.c_str();			// return user input to dataDisplay[4]
}
// Nitrate format validation
bool testNitrate(std::string inputTest)
{
	int inputLength;										// holds length of user input
	inputLength = inputTest.length();						// stores length of user input

	for (int count = 0; count < inputLength; count++)		// run for each char entered
	{
		if (!isdigit(inputTest.at(count)))					// test for each char entered is digit
			return false;									// return false if invalid
	}
	return true;											// return true if valid
}

// Filter function
void filterFunction(std::string filterDataDisplay[], std::string filterFileOutput[], int filter)
{
	std::string filterCleanInput;
	char filterClean;

	std::cout << "Was the filter cleaned?  Enter y or n: ";
	getline(std::cin, filterCleanInput);					// accept any user input as string

	while (!testFilter(filterCleanInput))					//  will not let user proceed until input is correct format
	{
		std::cout << "Invalid input.  Please enter a Y or N: ";
		getline(std::cin, filterCleanInput);				// accept any user input as string
	}

	filterClean = filterCleanInput.front();					// returns first character of string to char variable
	filterClean = toupper(filterClean);						// returns an uppercase character for formatting
	filterFileOutput[6] = filterClean;						// return user input to outputFile[6]
	filterDataDisplay[5] = filterClean;						// return user input to dataDisplay[5]
}
// Filter format validation
bool testFilter(std::string inputTest)
{
	if (inputTest.length() == 1)					// check if input is only one character
		if (inputTest == "Y" || inputTest == "y" || inputTest == "N" || inputTest == "n")	// validate single character
			return true;									// return true
		else return false;
	else return false;

}

// Gallons function
void gallonsFunction(std::string gallonsDataDisplay[], std::string gallonsFileOutput[], int gallons, double *gallonsTotal, double *gallonsChanged, double *gallonsPct)
{
	std::string gallonsInput;								// user input
	std::string percentDisplay;								// will contain percent changed and formatting

	std::cout << "How many gallons is your aquarium? ";
	//std::cin >> maxGallons;
	getline(std::cin, gallonsInput);
	while (!testGallons(gallonsInput))						//  will not let user proceed until input is correct format
	{
		std::cout << "Invalid input.  Enter total aquarium gallons: ";
		getline(std::cin, gallonsInput);					// accept any user input as string
	}
	*gallonsTotal = atof(gallonsInput.c_str());				// store user input to maxGallons in main() as double

	std::cout << "Enter total gallons of water changed: ";
	getline(std::cin, gallonsInput);
	while (!testGallons(gallonsInput))						//  will not let user proceed until input is correct format
	{
		std::cout << "Invalid input.  Enter gallons changed: ";
		getline(std::cin, gallonsInput);					// accept any user input as string
	}
	*gallonsChanged = atof(gallonsInput.c_str());			// store user input to gallonsChanged in main() as double

	*gallonsPct = (*gallonsChanged / *gallonsTotal) * 100;		// calculate percentage of water changed
	percentDisplay = std::to_string(*gallonsPct);			// write water change percentage to string
	percentDisplay.append(" % water changed");				// append for visual formatting in main display
	gallonsFileOutput[7] = std::to_string(*gallonsChanged);	// return gallonsChanged to outputFile[7]
	gallonsFileOutput[8] = std::to_string(*gallonsTotal);	// return gallonsTotal to outputFile[8]
	gallonsDataDisplay[6] = percentDisplay;					// return percentage to dataDisplay[6]
}
// Gallons format validation
bool testGallons(std::string inputTest)
{
	int inputLength;										// holds length of user input
	inputLength = inputTest.length();						// stores length of user input

	for (int count = 0; count < inputLength; count++)		// run for each char entered
	{
		if (!isdigit(inputTest.at(count)))					// test for each char entered is digit
			return false;									// return false if invalid
	}
	return true;											// return true if valid
}

// Journal Function
void journalFunction(std::string journalDataDisplay[], std::string journalFileOutput[], int journal)
{
	std::string logs;										// to hold user input as string
	std::string logsReturn;									// to hold value to return to main
	int logsLength;											// to hold length of logs
	std::cout << "Enter remarkable notes: " << std::endl;
	getline(std::cin, logs);								// get user input
	logsLength = logs.length();								// calculate length of journal entry
	logsReturn = std::to_string(logsLength);				// convert length to string
	logsReturn.append(" chars entered");					// append string literal to string
	journalFileOutput[9] = logs;							// return user input to outputFile[9]
	journalDataDisplay[7] = logsReturn.c_str();				// return user input metadata to dataDisplay[7]
}

//	Output to filestream
void fsOutputFunction(std::string writeFileOutput[], int primaryArray)
{

	std::cout << "Now writing to output file" << std::endl;
	std::ofstream outputFile;								// initialize output
	outputFile.open("aquarium.csv", std::ios::app);			// create the plaintext file aquarium.csv in the project directory or append new data if file exists

	writeFileOutput[0] = std::to_string(time(0));			// write the system time to fileOutput[0]

	for (int count = 0; count < MAIN_MENU; count++)			// write current data sets
	{
		outputFile << writeFileOutput[count] << "\t";		// tab delimited formatting for easy conversion in spreadsheet
	}
	outputFile << "\n";										// add newline for next run (not sure if neccessary testing required)
	outputFile.close();										// closes the output file
}

//Grades
void gradeFunction(double tempVal, double pHVal, double ammoniaVal, double nitriteVal, int nitrateVal, std::string filterVal[], int primaryArray, double gallonsTotVal, double gallonsChangedVal, double gallonsPctVal)
{
	/*
	The grading rubric is based on a combination common "best practices", the mean temperature and pH of the waters of the world where my
	fish hail from, and the API Freshwater Master Test Kit comparison chart.  The absolute values are calculated to determine by how much
	the user's input is off from ideal standards and I pulled the grading scales out of my butt.  The water change grade is broken and I'm
	not	sure why.  This program is still in alpha testing I suppose.
	*/

	double parameterDifferences[7];
	int parameterGrades[7];


	//	Temperature Scoring
	parameterDifferences[0] = abs(tempVal - TEMP_IDEAL);
	if (parameterDifferences[0] == 0)
		parameterGrades[0] = 100;
	else if (parameterDifferences[0] > 0 && parameterDifferences[0] <= 2)
		parameterGrades[0] = 90;
	else if (parameterDifferences[0] > 2 && parameterDifferences[0] <= 4)
		parameterGrades[0] = 80;
	else if (parameterDifferences[0] > 4 && parameterDifferences[0] <= 6)
		parameterGrades[0] = 70;
	else if (parameterDifferences[0] > 6 && parameterDifferences[0] <= 8)
		parameterGrades[0] = 60;
	else if (parameterDifferences[0] > 8 && parameterDifferences[0] <= 10)
		parameterGrades[0] = 50;
	else
		parameterGrades[0] = 0;
	std::cout << "Your temperature grade is " << parameterGrades[0] << "%" << std::endl;

	// pH Scoring
	parameterDifferences[1] = abs(pHVal - PH_IDEAL);
	if (parameterDifferences[1] == 0)
		parameterGrades[1] = 100;
	else if (parameterDifferences[1] > 0 && parameterDifferences[1] <= 0.2)
		parameterGrades[1] = 90;
	else if (parameterDifferences[1] > 0.2 && parameterDifferences[1] <= 0.4)
		parameterGrades[1] = 80;
	else if (parameterDifferences[1] > 0.4 && parameterDifferences[1] <= 0.6)
		parameterGrades[1] = 70;
	else if (parameterDifferences[1] > 0.6 && parameterDifferences[1] <= 0.8)
		parameterGrades[1] = 60;
	else if (parameterDifferences[1] > 0.8 && parameterDifferences[1] <= 1.0)
		parameterGrades[1] = 50;
	else
		parameterGrades[1] = 0;
	std::cout << "Your pH grade is " << parameterGrades[1] << "%" << std::endl;

	// Ammonia Scoring
	parameterDifferences[2] = abs(ammoniaVal - MIN_AMMONIA);
	if (parameterDifferences[2] == 0)
		parameterGrades[2] = 100;
	else if (parameterDifferences[2] > 0 && parameterDifferences[2] <= 0.25)
		parameterGrades[2] = 90;
	else if (parameterDifferences[2] > 0.25 && parameterDifferences[2] <= 0.5)
		parameterGrades[2] = 80;
	else if (parameterDifferences[2] > 0.5 && parameterDifferences[2] <= 1)
		parameterGrades[2] = 70;
	else if (parameterDifferences[2] > 1 && parameterDifferences[2] <= 2)
		parameterGrades[2] = 60;
	else if (parameterDifferences[2] > 2 && parameterDifferences[2] <= 4)
		parameterGrades[2] = 50;
	else
		parameterGrades[2] = 0;
	std::cout << "Your ammonia grade is " << parameterGrades[2] << "%" << std::endl;

	// Nitrite Scoring
	parameterDifferences[3] = abs(nitriteVal - MIN_NITRITE);
	if (parameterDifferences[3] == 0)
		parameterGrades[3] = 100;
	else if (parameterDifferences[3] > 0 && parameterDifferences[3] <= 0.25)
		parameterGrades[3] = 90;
	else if (parameterDifferences[3] > 0.25 && parameterDifferences[3] <= 0.5)
		parameterGrades[3] = 80;
	else if (parameterDifferences[3] > 0.5 && parameterDifferences[3] <= 1)
		parameterGrades[3] = 70;
	else if (parameterDifferences[3] > 1 && parameterDifferences[3] <= 2)
		parameterGrades[3] = 60;
	else if (parameterDifferences[3] > 2 && parameterDifferences[3] <= 4)
		parameterGrades[3] = 50;
	else
		parameterGrades[3] = 0;
	std::cout << "Your nitrite grade is " << parameterGrades[3] << "%" << std::endl;

	// Nitrate Scoring
	parameterDifferences[4] = abs(nitrateVal - MIN_NITRATE);
	if (parameterDifferences[4] == 0)
		parameterGrades[4] = 100;
	else if (parameterDifferences[4] > 0 && parameterDifferences[4] <= 5)
		parameterGrades[4] = 90;
	else if (parameterDifferences[4] > 5 && parameterDifferences[4] <= 10)
		parameterGrades[4] = 80;
	else if (parameterDifferences[4] > 10 && parameterDifferences[4] <= 20)
		parameterGrades[4] = 70;
	else if (parameterDifferences[4] > 20 && parameterDifferences[4] <= 40)
		parameterGrades[4] = 60;
	else if (parameterDifferences[4] > 40 && parameterDifferences[4] <= 80)
		parameterGrades[4] = 50;
	else
		parameterGrades[4] = 0;
	std::cout << "Your nitrate grade is " << parameterGrades[4] << "%" << std::endl;

	// Filter Scoring
	if (filterVal[5] == "Y")
		parameterGrades[5] = 100;
	else
		parameterGrades[5] = 0;
	std::cout << "Your filter grade is " << parameterGrades[5] << "%" << std::endl << std::endl;

	// Water Change Scoring
	std::cout << "Your ideal water change is " << (IDEAL_WATER_CHANGE * gallonsTotVal) << " gallons." << std::endl;
	std::cout << "You changed " << gallonsChangedVal << " gallons." << std::endl;
	std::cout << "That's " << gallonsPctVal << "% of the total." << std::endl;
	parameterDifferences[6] = abs((gallonsPctVal - (IDEAL_WATER_CHANGE * 100)));
	std::cout << parameterDifferences[6] << "% difference." << std::endl;
	//cout << waterScore << endl;
	if (parameterDifferences[6] == 0)
		parameterGrades[6] = 100;
	else if (parameterDifferences[6] > 0 && parameterDifferences[6] <= 5)
		parameterGrades[6] = 95;
	else if (parameterDifferences[6] > 5 && parameterDifferences[6] <= 10)
		parameterGrades[6] = 90;
	else if (parameterDifferences[6] > 10 && parameterDifferences[6] <= 15)
		parameterGrades[6] = 80;
	else if (parameterDifferences[6] > 15 && parameterDifferences[6] <= 20)
		parameterGrades[6] = 70;
	else if (parameterDifferences[6] > 20 && parameterDifferences[6] <= 25)
		parameterGrades[6] = 50;
	else
		parameterGrades[6] = 0;
	std::cout << "Your water change grade is " << parameterGrades[6] << "%" << std::endl << std::endl;  // This is working, yay!

	// Averaging scores
	std::string personalityAssessment;				// shows string literal based on aquariumScore
	int aquariumScore;								// holds arbitrary number as average of all grades
	int aquariumTotal = 0;							// holds arbitrary number as total of all grades

	for (int count : parameterGrades)
		aquariumTotal += parameterGrades[count];	// get total
	aquariumScore = aquariumTotal / 7;				// get average

	std::cout << "Your aquarium score is: " << aquariumScore << "%!" << std::endl << std::endl;
	if (aquariumScore == 100)
	{
		personalityAssessment = "Angelfish";
		std::cout << "Congratulations, you're an Angelfish!" << std::endl;
	}
	else if (aquariumScore < 100 && aquariumScore >= 90)
	{
		personalityAssessment = "Pacu";
		std::cout << "You're a Pacu!" << std::endl;
	}
	else if (aquariumScore < 90 && aquariumScore >= 80)
	{
		personalityAssessment = "Boita";
		std::cout << "You're a Boita!" << std::endl;
	}
	else if (aquariumScore < 80 && aquariumScore >= 70)
	{
		personalityAssessment = "Plecostomus";
		std::cout << "You're a Plecostomus.." << std::endl;
	}
	else if (aquariumScore < 70 && aquariumScore >= 60)
	{
		personalityAssessment = "Aquatic Plant";
		std::cout << "You're an aquatic plant...?" << std::endl;
	}
	else if (aquariumScore < 60 && aquariumScore >= 60)
	{
		personalityAssessment = "Log";
		std::cout << "You're a log... bummer." << std::endl;
	}
	else
	{
		personalityAssessment = "Maladroit";
		std::cout << "Maybe consider another hobby?  I hear ASCII artists are in demand.\n\nIn fact, I know a guy." << std::endl << std::endl;
	}
	system("pause");
}

// Help
void showHelp(std::string readFileInput[], int primaryArray)
{
	int count = 0;												// loop counter variable
	std::ifstream outputFile;								// initialize output
	outputFile.open("aquarium.csv");						// open aquarium.csv in the project directory

	
	while (count < primaryArray && outputFile >> readFileInput[count])			// read first eleven rows
		count++;
	
	outputFile.close();										// closes the output file
}

//	Transition art.
void artRoll()
{
	int artRollMiddle;			// initialize int to hold PRN
	int artRollBottom;			// initialize int to hold PRN
	unsigned seed = time(0);	// Get system time
	srand(seed);				// Use the time to seed PRNG

	artUpper0();				// only one of these for now

	artRollMiddle = (rand() % (ART_MAX - ART_MIN + 1)) + ART_MIN;	// rolls the "dice" to see what art will be shown.
	if (artRollMiddle == 1)
	{
		artMiddle0();			// static art
	}
	else if (artRollMiddle == 2)
	{
		artMiddle1();			// static art
	}
	else
	{
		artMiddle2();			// static art
	}
	artRollBottom = (rand() % (ART_MAX - ART_MIN + 1)) + ART_MIN;	// rolls the "dice" to see what art will be shown.
	if (artRollBottom == 1)
	{
		artBottom0();			// static art
	}
	else if (artRollBottom == 2)
	{
		artBottom1();			// static art
	}
	else
	{
		artBottom2();			// static art
	}
}
void art0()
{
	using namespace std;
	// Standard console screen will display 78 chars wide by 25 lines tall.
	cout << "|        o                                                                    |" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|         o o                                                                 |" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|         o                                                                   |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|         o                                                                   |" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
	cout << "|         o                                                                   |" << endl; // 78 chars wide
	cout << "|         o                                                                   |" << endl; // 78 chars wide
	cout << "|         o                                                                   |" << endl; // 78 chars wide
	cout << "|        o                                                                    |" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|           o                                                                 |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
	cout << "|        oo                                                                   |" << endl; // 78 chars wide
}
void artUpper0()
{
	using namespace std;
	// Standard console screen will display 78 chars wide by 25 lines tall.
	cout << "|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|         o o                                                                 |" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|         o                                                                   |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|         o                                                                   |" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
	cout << "|         o                                                                   |" << endl; // 78 chars wide
	cout << "|         o                                                                   |" << endl; // 78 chars wide
	cout << "|         o                                                                   |" << endl; // 78 chars wide
	cout << "|        o                                                                    |" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|           o                                                                 |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
	cout << "|        oo                                                                   |" << endl; // 78 chars wide
}
void artMiddle0()
{
	using namespace std;
	// Begin transition art 1.  Standard console screen will display 78 chars wide by 25 lines tall.
	cout << "|        o                                                                    |" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|         o                                                                   |" << endl; // 78 chars wide
	cout << "|                                             ______                          |" << endl; // 78 chars wide
	cout << "|          o                                 /   (                            |" << endl; // 78 chars wide
	cout << "|         o                                 /  -(                             |" << endl; // 78 chars wide
	cout << "|          o                               /   (                              |" << endl; // 78 chars wide
	cout << "|                                       __/  -(__    _                        |" << endl; // 78 chars wide
	cout << "|          o                           /         \\  / |                       |" << endl; // 78 chars wide
	cout << "|         o                           /  O        \\/ /                        |" << endl; // 78 chars wide
	cout << "|                                     >             |                         |" << endl; // 78 chars wide
	cout << "|         o                           \\           /\\ \\                        |" << endl; // 78 chars wide
	cout << "|         o                            \\_____   _/  \\_|                       |" << endl; // 78 chars wide
	cout << "|         o                               /  \\ |                              |" << endl; // 78 chars wide
	cout << "|        o                                |   \\|                              |" << endl; // 78 chars wide
	cout << "|                                         \\                                   |" << endl; // 78 chars wide
	cout << "|          o                               \\                                  |" << endl; // 78 chars wide
	cout << "|           o                               \\                                 |" << endl; // 78 chars wide
	cout << "|          o                                 \\                                |" << endl; // 78 chars wide
	cout << "|         o                                   \\___                            |" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
	cout << "|        o                                                                    |" << endl; // 78 chars wide
}
void artMiddle1()
{
	using namespace std;
	// Begin transition art 4.  Standard console screen will display 78 chars wide by 25 lines tall.
	cout << "|        o                                                                    |" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|         o                                                                   |" << endl; // 78 chars wide
	cout << "|                                            _                                |" << endl; // 78 chars wide
	cout << "|          o                                 |\\                               |" << endl; // 78 chars wide
	cout << "|         o                        _    _,.--/ \\--.,_                         |" << endl; // 78 chars wide
	cout << "|          o                       }\\  /__________  o\\                        |" << endl; // 78 chars wide
	cout << "|                                   >><   -------- (  <                       |" << endl; // 78 chars wide
	cout << "|          o                       }/  \\_           _/                        |" << endl; // 78 chars wide
	cout << "|         o                              `\"-=.,,.=-\"                          |" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
	cout << "|         o                                                                   |" << endl; // 78 chars wide
	cout << "|         o                                                                   |" << endl; // 78 chars wide
	cout << "|         o                                                                   |" << endl; // 78 chars wide
	cout << "|        o                                                                    |" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|           o                                                                 |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|         o                                                                   |" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
	cout << "|        o                                                                    |" << endl; // 78 chars wide
}
void artMiddle2()
{
	using namespace std;
	// Begin transition art 6.  Standard console screen will display 78 chars wide by 25 lines tall.
	cout << "|        o                                                                    |" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
	cout << "|          o                      \\ /--                                       |" << endl; // 78 chars wide
	cout << "|          o                       X                                          |" << endl; // 78 chars wide
	cout << "|         o                       / \\--                                       |" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
	cout << "|          o                   (crappy floating                               |" << endl; // 78 chars wide
	cout << "|         o                            plant part)                            |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|         o                                                                   |" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
	cout << "|         o                                                                   |" << endl; // 78 chars wide
	cout << "|         o                                                                   |" << endl; // 78 chars wide
	cout << "|         o                                                                   |" << endl; // 78 chars wide
	cout << "|        o                                                                    |" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|           o                                                                 |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|         o                                                                   |" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
	cout << "|        o                                                                    |" << endl; // 78 chars wide
}
void artBottom0()
{
	using namespace std;
	// Begin transition art 2.  Standard console screen will display 78 chars wide by 25 lines tall.
	cout << "|        o                                                                    |" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
	cout << "|          o                      \\ /--                                       |" << endl; // 78 chars wide
	cout << "|          o                       X                                          |" << endl; // 78 chars wide
	cout << "|         o                       /|\\--                                       |" << endl; // 78 chars wide
	cout << "|                                  |                                          |" << endl; // 78 chars wide
	cout << "|          o                       |                                          |" << endl; // 78 chars wide
	cout << "|         o                        |                                          |" << endl; // 78 chars wide
	cout << "|          o                       |        \\ /--                             |" << endl; // 78 chars wide
	cout << "|          o                       |   ------X                                |" << endl; // 78 chars wide
	cout << "|         o                        |  /     / \\--                             |" << endl; // 78 chars wide
	cout << "|         o                        | /                                        |" << endl; // 78 chars wide
	cout << "|                                  |/                                         |" << endl; // 78 chars wide
	cout << "|         o                        /                                          |" << endl; // 78 chars wide
	cout << "|         o                       /                                           |" << endl; // 78 chars wide
	cout << "|         o                      /                                            |" << endl; // 78 chars wide
	cout << "|        o                      /                                             |" << endl; // 78 chars wide
	cout << "|                              /                                              |" << endl; // 78 chars wide
	cout << "|          o                   |                                              |" << endl; // 78 chars wide
	cout << "|           o                  |                                              |" << endl; // 78 chars wide
	cout << "|          o                 \\|/--                                            |" << endl; // 78 chars wide
	cout << "|          o                  X                                               |" << endl; // 78 chars wide
	cout << "|         o                  /|\\--                                            |" << endl; // 78 chars wide
	cout << "|        o                    |                                               |" << endl; // 78 chars wide}
}
void artBottom1()
{
	using namespace std;
	// Begin transition art 3.  Standard console screen will display 78 chars wide by 25 lines tall.
	cout << "|        o                                                                    |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|         o                                                 /\\___\\/           |" << endl; // 78 chars wide
	cout << "|                                             \\\\        /--/_____/            |" << endl; // 78 chars wide
	cout << "|          o                                   \\\\      //--                   |" << endl; // 78 chars wide
	cout << "|         o                              \\\\     |     //                      |" << endl; // 78 chars wide
	cout << "|          o                              \\\\   \\\\    //                       |" << endl; // 78 chars wide
	cout << "|                                          |    \\\\  //                        |" << endl; // 78 chars wide
	cout << "|          o              |                |     | / |\\                       |" << endl; // 78 chars wide
	cout << "|         o               \\\\____          \\\\ |   \\|  |                        |" << endl; // 78 chars wide
	cout << "|                               \\\\         \\\\|   || /                         |" << endl; // 78 chars wide
	cout << "|         o                      \\\\         /\\ | \\| \\                         |" << endl; // 78 chars wide
	cout << "|         o                       \\ \\ _____/   V     \\                        |" << endl; // 78 chars wide
	cout << "|         o                        \\__\\_ ___/\\        \\                       |" << endl; // 78 chars wide
	cout << "|        o                             //     \\        \\                      |" << endl; // 78 chars wide
	cout << "|                  (it's a branch     //       \\       |                      |" << endl; // 78 chars wide
	cout << "|          o          or something)  //         \\       \\                     |" << endl; // 78 chars wide
	cout << "|           o                       //           \\       \\                    |" << endl; // 78 chars wide
	cout << "|          o                       //             \\       \\                   |" << endl; // 78 chars wide
	cout << "|         o                       //               \\    _,.\\-,.-;,;;+-=:(&^$%]|" << endl; // 78 chars wide
	cout << "|                                //    _,.,-,.-;,;;+\\=:(&^$%^*(&%#%&)#:!))^%}]|" << endl; // 78 chars wide
	cout << "|________o____________.,.,-,.-;,//+-=:(&^$%^*(&%&^$%^*/&%&^$%^*(%%!#&[#%&)#:)]|" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
}
void artBottom2()
{
	using namespace std;
	// Begin transition art 5.  Standard console screen will display 78 chars wide by 25 lines tall.
	cout << "|        o                                                                    |" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|         o                                                                   |" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|         o                                                                   |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
	cout << "|          o                                                                  |" << endl; // 78 chars wide
	cout << "|         o                                                                   |" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
	cout << "|         o                                                                   |" << endl; // 78 chars wide
	cout << "|         o                                                                   |" << endl; // 78 chars wide
	cout << "|         o                                                                   |" << endl; // 78 chars wide
	cout << "|        o                                                                    |" << endl; // 78 chars wide
	cout << "|                                                                             |" << endl; // 78 chars wide
	cout << "|          o         _                                                        |" << endl; // 78 chars wide
	cout << "|           o       _ \\                                                       |" << endl; // 78 chars wide
	cout << "|          o       / \\|_,.~-.,_                                               |" << endl; // 78 chars wide
	cout << "|         o           8~       *^%\\\\                                          |" << endl; // 78 chars wide
	cout << "|                     `-mmm),.-~^` `)                                         |" << endl; // 78 chars wide
	cout << "|        o              \\\\\\\\       /                                          |" << endl; // 78 chars wide
}


// debug function

// uncomment function, prototype, and function call in main() to cause default switch case to return values stored in array written to file
//void debugDataArrayFunction(std::string showFileOutput[], int primaryArray)
//{
//	for (int count = 0; count < primaryArray; count++)		// consider updating to range based for loop
//		std::cout << count << " " << showFileOutput[count] << std::endl;
//	std::cout << std::endl;
//}